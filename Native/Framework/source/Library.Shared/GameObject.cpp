#include "pch.h"
#include "GameObject.h"

using namespace std;

namespace Library
{
	RTTI_DEFINITIONS(GameObject)

	GameObject::GameObject(Game& game) :
		DrawableGameComponent(game)
	{
	}

	GameObject::GameObject(Game& game, const shared_ptr<Camera>& camera) :
		DrawableGameComponent(game, camera)
	{
	}

	void GameObject::AddComponent(const shared_ptr<GameComponent>& component)
	{
		uint64_t typeId = component->TypeIdInstance();
		auto result = mComponents.emplace(typeId, vector<shared_ptr<GameComponent>>());		
		vector<shared_ptr<GameComponent>>& components = result.first->second;		 
		if (result.second == false)
		{
			auto it = find(components.cbegin(), components.cend(), component);
			if (it != components.cend())
			{
				throw GameException("This component already exists within this GameObject.");
			}

			components.push_back(component);
		}
	}

	shared_ptr<GameComponent> GameObject::GetComponent(const uint64_t typeId)
	{
		auto it = mComponents.find(typeId);
		if (it != mComponents.cend())
		{
			vector<shared_ptr<GameComponent>>& components = it->second;
			if (components.size() > 0)
			{
				return components.front();
			}
		}

		return nullptr;
	}

	void GameObject::Initialize()
	{
		for (auto& componentSet : mComponents)
		{
			auto& components = componentSet.second;
			for (auto& component : components)
			{
				component->Initialize();
			}
		}
	}

	void GameObject::Update(const GameTime& gameTime)
	{
		for (auto& componentSet : mComponents)
		{
			auto& components = componentSet.second;
			for (auto& component : components)
			{
				if (component->Enabled())
				{
					component->Update(gameTime);
				}
			}
		}
	}

	void GameObject::Draw(const GameTime& gameTime)
	{
		for (auto& componentSet : mComponents)
		{
			auto& components = componentSet.second;
			for (auto& component : components)
			{
				DrawableGameComponent* drawableGameComponent = component->As<DrawableGameComponent>();
				if (drawableGameComponent != nullptr && drawableGameComponent->Visible())
				{
					drawableGameComponent->Draw(gameTime);
				}
			}
		}
	}
}
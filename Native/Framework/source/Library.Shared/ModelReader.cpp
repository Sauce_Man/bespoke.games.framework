#include "pch.h"
#include "ModelReader.h"

using namespace std;

namespace Library
{
	RTTI_DEFINITIONS(ModelReader)

	ModelReader::ModelReader(Game& game) :
		ContentTypeReader(game, Model::TypeIdClass())
	{
	}

	shared_ptr<Model> ModelReader::_Read(const wstring& assetName)
	{
		return shared_ptr<Model>(new Model(Utility::ToString(assetName)));
	}
}